#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>
#include <QQmlContext>
#include <QQmlEngine>
#include "Task.h"
#include "Project.h"
#include "ObservableTaskList.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<Task>("fr.uca.iut", 1, 0, "Task");
    qmlRegisterType<Project>("fr.uca.iut", 1, 0, "Project");
    qmlRegisterType<ObservableTaskList>("fr.uca.iut", 1, 0, "TaskModel");

    ObservableTaskList model;
    model.addTask(new Task("Example Task 1", "This is an example task.", 2));
    model.addTask(new Task("Example Task 2", "This is another example task.", 1));
    QGuiApplication *app = SailfishApp::application(argc,argv);
    QQuickView *view = SailfishApp::createView();
    Project* project = new Project("Example Project", &model);
    view->rootContext()->setContextProperty("projectModel", project);
    view->setSource(SailfishApp::pathTo("qml/oh_the_things_you_ll_do.qml"));
    view->show();
    return app->exec();
}
