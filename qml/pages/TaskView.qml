import QtQuick 2.0
import Sailfish.Silica 1.0
import fr.uca.iut 1.0

Page {
    id: taskView

    property var taskModel

    SilicaFlickable {
        anchors.fill: parent

        Column {
            width: parent.width - 2 * Theme.paddingLarge
            spacing: Theme.paddingLarge
            anchors {
                horizontalCenter: parent.horizontalCenter
                margins: Theme.paddingLarge
            }

            // this is to provide some top margin, to make the view easier to look at when a banner notification happens
            Item {
                width: parent.width
                height: 2 * Theme.paddingLarge
            }

            TextField {
                id: titleField
                text: taskModel.title
                label: qsTr("Title")
                font.pixelSize: Theme.fontSizeExtraLarge
                onTextChanged: taskModel.title = text
            }

            TextField {
                id: descriptionField
                text: taskModel.description
                label: qsTr("Description")
                font.pixelSize: Theme.fontSizeLarge
                onTextChanged: taskModel.description = text
            }

            TextField {
                id: priorityField
                text: taskModel.priority.toString()
                label: qsTr("Priority")
                validator: IntValidator { bottom: 0; top: 3 }
                onTextChanged: taskModel.priority = parseInt(text)
            }
        }
    }
}
